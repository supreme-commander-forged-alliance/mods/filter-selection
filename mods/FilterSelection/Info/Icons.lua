-- Within the icons folder (\Mods\FilterSelection\textures\icons) you can find all the available icons to this mod.
-- You can add your own icons too, simply place them within the icons folder.
	-- When adding your own, make sure the icon is of DDS format (DXT5, ARGB, 8BPP and Interpolated Alpha)
	-- Other formats are runnable by the engine, but are not supported by this mod!
	-- There is a Photoshop Plugin by NVIDIA to open and save the DDS format: https://developer.nvidia.com/nvidia-texture-tools-adobe-photoshop

-- You should add in the full name of the icon within the icon variable, but without the extension!
-- This means that...
-- air.dds -> air
-- ope3001_icon.dds -> ope3001_icon

-- The icons of all the default supreme commander units have 'weird' names.
-- These are the internal names within the engine.
-- You can (generally) view these by showing the items within the folder as large items.