
-- Below you find a list of all the usefull categories within Supreme Commander.
-- These categories are used to determine what units should and should not be filtered out.

-- As an example, a T1 UEF Interceptor has the following categories:
	-- categories.AIR, categories.UEF, categories.ANTIAIR, categories.TECH1

-- You can perform operations on these categories.
	-- +, this means either. Every unit that has one of the two categories will be included in the filter.
	-- *, this means both. Every unit that has both the categories will be included.
	-- -, this means not. Every unit that has this category will not be included.

-- As an example:
	-- categories.AIR + categories.LAND 						-> Filters all the units which are either aerial or land.
	-- categories.AIR * categories.LAND 						-> Filters all the units that are both aerial and land (none exist).
	-- categories.AIR * categories.ANTIAIR * categories.TECH3 	-> Filters all the units that are aerial, considered anti air and are of tech 3. Aka: ASF's (Air Supiority fighters)
	-- categories.AIR * categories.TECH3 * categories.BOMBER 	-> Filters all the units that are aerial, are of tech 3 and are considered bombers. Aka: t3 bombers.
	-- categories.LAND - categories.SHIELD 						-> Filters all the land units that are not mobile shields.
	-- categories.LAND * categories.TECH2 - categories.SHIELD 	-> Filters all the land units that are tech 2, but not mobile shields.

-- I hope these examples are enough to show you how to filter the units that you want to filter.

-- Disclaimer: With custom mods, not every unit will have the categorie set. 
-- This is something I cannot program up against, when other mods do not get filtered it means the authors of those mods need to fix the categories on their units.

categories.ABILITYBUTTON
categories.AEON
categories.AIR
categories.AIRSTAGINGPLATFORM
categories.ALLPROJECTILES
categories.ALLUNITS
categories.ANTIAIR
categories.ANTIMISSILE
categories.ANTINAVY
categories.ANTISUB
categories.ANTITORPEDO
categories.ARTILLERY
categories.BATTLESHIP
categories.BENIGN
categories.BOMBER
categories.BOT
categories.BUILTBYCOMMANDER
categories.BUILTBYEXPERIMENTALSUB
categories.BUILTBYLANDTIER2FACTORY
categories.BUILTBYLANDTIER3FACTORY
categories.BUILTBYQUANTUMGATE
categories.BUILTBYTIER1ENGINEER
categories.BUILTBYTIER1FACTORY
categories.BUILTBYTIER2COMMANDER
categories.BUILTBYTIER2ENGINEER
categories.BUILTBYTIER2FACTORY
categories.BUILTBYTIER3COMMANDER
categories.BUILTBYTIER3ENGINEER
categories.BUILTBYTIER3FACTORY
categories.CANNOTUSEAIRSTAGING
categories.CANTRANSPORTCOMMANDER
categories.CAPTURE
categories.CARRIER
categories.CIVILIAN
categories.CIVILLIAN
categories.COMMAND
categories.CONSTRUCTION
categories.CONSTRUCTIONSORTDOWN
categories.COUNTERINTELLIGENCE
categories.CRUISER
categories.CYBRAN
categories.Cybran
categories.DEFENSE
categories.DEFENSIVEBOAT
categories.DESTROYER
categories.DIRECTFIRE
categories.DRAGBUILD
categories.ECONOMIC
categories.ENERGYPRODUCTION
categories.ENERGYSTORAGE
categories.ENGINEER
categories.ENGINEERSTATION
categories.EXPERIMENTAL
categories.FACTORY
categories.FAVORSWATER
categories.FERRYBEACON
categories.FIELDENGINEER
categories.FRIGATE
categories.GATE
categories.GROUNDATTACK
categories.HIGHALTAIR
categories.HIGHPRIAIR
categories.HOVER
categories.HYDROCARBON
categories.INDIRECTFIRE
categories.INSIGNIFICANTUNIT
categories.INTEL
categories.INTELLIGENCE
categories.INVULNERABLE
categories.LAND
categories.LIGHTBOAT
categories.MASSEXTRACTION
categories.MASSFABRICATION
categories.MASSPRODUCTION
categories.MASSSTORAGE
categories.MISSILE
categories.MOBILE
categories.MOBILESONAR
categories.NAVAL
categories.NAVALCARRIER
categories.NEEDMOBILEBUILD
categories.NOFORMATION
categories.NOSPLASHDAMAGE
categories.NUKE
categories.NUKESUB
categories.OMNI
categories.OPERATION
categories.OPTICS
categories.ORBITALSYSTEM
categories.OVERLAYANTIAIR
categories.OVERLAYANTINAVY
categories.OVERLAYCOUNTERINTEL
categories.OVERLAYDEFENSE
categories.OVERLAYDIRECTFIRE
categories.OVERLAYINDIRECTFIRE
categories.OVERLAYMISC
categories.OVERLAYOMNI
categories.OVERLAYRADAR
categories.OVERLAYSONAR
categories.PATROLHELPER
categories.POD
categories.PODSTAGINGPLATFORM
categories.PRODUCTDL
categories.PRODUCTFA
categories.PRODUCTSC1
categories.PROJECTILE
categories.RADAR
categories.RALLYPOINT
categories.REBUILDER
categories.RECLAIM
categories.RECLAIMABLE
categories.RECLAIMFRIENDLY
categories.REPAIR
categories.SATELLITE
categories.SCOUT
categories.SELECTABLE
categories.SERAPHIM
categories.SHIELD
categories.SHOWATTACKRETICLE
categories.SHOWQUEUE
categories.SILO
categories.SIZE12
categories.SIZE16
categories.SIZE20
categories.SIZE4
categories.SIZE8
categories.SONAR
categories.SORTCONSTRUCTION
categories.SORTDEFENSE
categories.SORTECONOMY
categories.SORTINTEL
categories.SORTSTRATEGIC
categories.SPECIALHIGHPRI
categories.SPECIALLOWPRI
categories.STATIONASSISTPOD
categories.STRATEGIC
categories.STRUCTURE
categories.SUBCOMMANDER
categories.SUBMERSIBLE
categories.T1SUBMARINE
categories.T2SUBMARINE
categories.TACTICAL
categories.TACTICALMISSILEPLATFORM
categories.TANK
categories.TARGETCHASER
categories.TECH1
categories.TECH2
categories.TECH3
categories.TELEPORTBEACON
categories.TORPEDO
categories.TRANSPORTATION
categories.TRANSPORTBUILTBYTIER1FACTORY
categories.TRANSPORTBUILTBYTIER2FACTORY
categories.TRANSPORTBUILTBYTIER3FACTORY
categories.TRANSPORTFOCUS
categories.UEF
categories.UNSELECTABLE
categories.UNTARGETABLE
categories.VERIFYMISSILEUI
categories.VISIBLETORECON
categories.WALL