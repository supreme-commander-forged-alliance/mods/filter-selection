do

	-- import("/lua/lazyvar.lua").ExtendedErrorMessages = true

   	local originalCreateUI = CreateUI

   	function CreateUI(isReplay)
      	originalCreateUI(isReplay)

      	if not isReplay then
      		import("/mods/FilterSelection/modules/Userinterface.lua").CreateUI(import('/lua/ui/game/borders.lua').GetMapGroup())
      		import("/mods/FilterSelection/modules/Entrypoint.lua").OnStart()
  		end	
   	end
end
