-- Incase a sudden name change.
local FilePath = "FilterSelection"

-- Import required modules.
local userinterface = import("/mods/" .. FilePath.. "/Modules/Userinterface.lua")

functionality 		= "filterout"
local filters 		= {}
local appliedFilter = nil

-- Gets called from EntryPoint.LUA.
function OnStart()

	-- Retrieve the filters.
	fil = import("/mods/".. FilePath .. "/config.lua").filters

	-- Add the individual filters. 
	for c, f in fil do
		filters[c] = { name = f.name, cat = f.cat, active = false }
	end

end

-- Gets called from the hotkeys or from the filter buttons.
function ToggleFilter(index)

	-- Toggle the filter.
	filters[index].active = not filters[index].active
	userinterface.ChangeUI(index, filters[index].active)

	appliedFilter = nil

end

function GenerateNewFilter()

	if(functionality == "filterout") then

		appliedFilter = categories.ALLUNITS

		for c, f in filters do

			if(f.active) then
				appliedFilter = appliedFilter - f.cat
			end
		end

	else

		appliedFilter = categories.UNSELECTABLE

		for c, f in filters do

			if(f.active) then
				appliedFilter = appliedFilter + f.cat
			end
		end
	end
end

-- This function gets called through the hotkeys.
function ResetFilter()

	for index, filter in filters do
		filter.active = false
		userinterface.ChangeUI(index, false)
	end

	appliedFilter = nil

end

function SwitchFilter()

	if functionality == "filterout" then
		functionality = "filterin"
	else
		functionality = "filterout"
	end

	for index, filter in filters do
		userinterface.ChangeUI(index, filter.active)
	end
	appliedFilter = nil

end

function Filter()

	if appliedFilter == nil then
		GenerateNewFilter()
	end

	local unitArray = GetSelectedUnits()
	unitArray = EntityCategoryFilterDown(appliedFilter, unitArray)
	SelectUnits(unitArray)

end