local helpFilter = import("/Mods/FilterSelection/Modules/HelpModules/HelpFilterButtons.lua")
local LayoutHelpers = import('/lua/maui/layouthelpers.lua') 
local config = import("/Mods/FilterSelection/config.lua")

local helpUI = import("/Mods/FilterSelection/Modules/HelpModules/HelpUserinterface.lua")
local helpTex = import("/Mods/FilterSelection/Modules/HelpModules/HelpTexture.lua")

local bracketlayout = import("/Mods/FilterSelection/Textures/BracketDimensions.lua").layout
local tooltip = import("/lua/ui/game/tooltip.lua")
UIFilters = { }

function CreateUI(parent)

	-- Retrieve the faction.
	local armyTable = GetArmiesTable().armiesTable
	local factionNumber = armyTable[GetFocusArmy()].faction
	local Factions = {'UEF', 'Aeon', 'Cybran', 'Seraphim'};
	local faction = Factions[factionNumber + 1]

	-- Create the open / close button.
	ButtonOpen = helpUI.CreateButton(
		parent, 
		helpTex.GetPathFactionUI(faction, "tab", "openup"),
		helpTex.GetPathFactionUI(faction, "tab", "openover"),
		helpTex.GetPathFactionUI(faction, "tab", "opendown"),
		helpTex.GetPathFactionUI(faction, "tab", "opendis")
		)

	ButtonClose = helpUI.CreateButton(
		parent, 
		helpTex.GetPathFactionUI(faction, "tab", "closeup"),
		helpTex.GetPathFactionUI(faction, "tab", "closeover"),
		helpTex.GetPathFactionUI(faction, "tab", "closedown"),
		helpTex.GetPathFactionUI(faction, "tab", "closedis")
		)

	tooltip.AddButtonTooltip(ButtonClose, {
        text = '[Hide/Show] Filters Bar' ,
        body = '',
    })

    tooltip.AddButtonTooltip(ButtonOpen, {
        text = '[Hide/Show] Filters Bar' ,
        body = '',
    })

	-- Set the functionality.
	ButtonClose.Depth:Set(100)
	ButtonClose.OnClick = function (self, modifiers)
		ButtonClose:Hide()
		ButtonOpen:Show()
	end

	ButtonOpen.Depth:Set(100)
	ButtonOpen.OnClick = function (self,modifiers)
		ButtonOpen:Hide()
		ButtonClose:Show()
	end

	ButtonOpen:Hide()

	-- Position the buttons.
	LayoutHelpers.AtRightTopIn(ButtonOpen, parent, config.startX , -3)
	LayoutHelpers.AtRightTopIn(ButtonClose, parent, config.startX , -3)

	-- Create the filters from the config file.
	for c, filter in config.filters do

		UIFilters[c] = helpFilter.CreateFilterButton(ButtonClose, filter.icon, filter.multiplieicons)
		UIFilters[c].Number = c
		UIFilters[c].OnClick = function (self, event)
			import("/mods/FilterSelection/modules/Functionality.lua").ToggleFilter(self.Number)
		end
		LayoutHelpers.AtRightTopIn(UIFilters[c], ButtonClose, (c - 1) * 41 - 3 , 29)

    	tooltip.AddButtonTooltip(UIFilters[c], {
        	text = filter.name,
        	body = filter.description,
    	})

    end 

	-- Create the rest of the interface.
	brackleft = helpUI.CreateBitmap(ButtonClose, helpTex.GetPathFactionUI(faction, "bracket", "left"))
	brackleft.Depth:Set(0)
	LayoutHelpers.AtRightTopIn(brackleft, ButtonClose, -23 , 3)

	-- While loop the middle.
	local startX = 43
	local currentX = 43

	while(currentX < startX + 40 * (table.getn(config.filters) + 0.5) - bracketlayout['bracket_bmp_t'].width) do
		local bit = helpUI.CreateBitmap(ButtonClose, helpTex.GetPathFactionUI(faction, "bracket", "middle"))
		LayoutHelpers.AtRightTopIn(bit, ButtonClose, currentX, 10)
		bit.Depth:Set(0)

		currentX = currentX + bracketlayout["bracket_bmp_m"].width - 2
	end

	brackright = helpUI.CreateBitmap(ButtonClose, helpTex.GetPathFactionUI(faction, "bracket", "right"))
	brackright.Depth:Set(0)
	LayoutHelpers.AtRightTopIn(brackright, ButtonClose, currentX , 3)
end

function ChangeUI(index, state)
	UIFilters[index].Active = state
	UIFilters[index].ChangeState()
end