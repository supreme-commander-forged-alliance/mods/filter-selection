-- Incase a sudden name change.
local FilePath = "FilterSelection"

-- Import other required modules of the mod.
local config = import("/mods/" .. FilePath .. "/config.lua")
local func = import("/mods/" .. FilePath .. "/modules/functionality.lua")

-- This function gets called from (aka: hooked to) gamemain.lua in the initialisation method.
function OnStart()
	AddKeyBindings()
	func.OnStart()
end

function AddKeyBindings()

	-- Add the filter action keybinding.
	local newKeyBindings =  
	{ 
		[config.filterKeybinding] = {action = 'UI_Lua import("/mods/FilterSelection/modules/Functionality.lua").Filter()'},
		[config.filterReset] = {action = 'UI_Lua import("/mods/FilterSelection/modules/Functionality.lua").ResetFilter()'},
		[config.filterSwitch] = {action = 'UI_Lua import("/mods/FilterSelection/modules/Functionality.lua").SwitchFilter()'},
	}

	-- Retrieve the filters.
	local filters = config.filters

	-- Add the individual filter keybindings.
	for c, filter in filters do
		if not (filter.keybinding == "") then
			newKeyBindings[filter.keybinding] = {action =	 'UI_Lua import("/mods/FilterSelection/modules/Functionality.lua").ToggleFilter('.. tostring(c) ..')' }
		end
	end

    -- Add the new keybindings.
    IN_AddKeyMapTable(newKeyBindings)

end