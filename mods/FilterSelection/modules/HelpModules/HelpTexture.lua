local filepath = "/mods/FilterSelection/textures/"

local textureLookupTable = 
{
	['bracket'] = 
	{
		["right"] = { "bracket_bmp_b.dds" },
		["middle"] = {"bracket_bmp_m.dds"},
		["left"] = {"bracket_bmp_t.dds"},
	},

	['tab'] = 
	{
		["closedis"] = {"tab-close_btn_dis.dds"},
		["closedown"]  = {"tab-close_btn_down.dds"},
		["closeover"]  = {"tab-close_btn_over.dds"},
		["closeup"]  = {"tab-close_btn_up.dds"},
		["opendis"]  = {"tab-open_btn_dis.dds"},
		["opendown"]  = {"tab-open_btn_down.dds"},
		["openover"]  = {"tab-open_btn_over.dds"},
		["openup"]  = {"tab-open_btn_up.dds"},
	},
}

textureNeutral = filepath .. "FilterNeutral.dds"
textureFilterOut = filepath .. "FilterOut.dds"
textureFilterIn = filepath.. "FilterIn.dds"
textureSwitchOut = filepath .. "IconFIlterOut.dds"
textureSwitchIn = filepath .. "IconFIlterIn.dds"

textureGenericNormal = filepath.."generic_normal.dds"
textureGenericUp = filepath.."generic_up.dds"
textureGenericDown = filepath.."generic_down.dds"
textureGenericOver = filepath.."generic_over.dds"

-- Retrieves a part of the faction dependant UI.
function GetPathFactionUI(faction, typePart, part)
	return filepath .. faction .. "/" .. typePart .. "/" .. textureLookupTable[typePart][part][1]
end

-- Retrieves a single icon, for a bitmap.
function GetPathIcon(iconname)
	return filepath .. "icons/" .. iconname .. ".dds"
end