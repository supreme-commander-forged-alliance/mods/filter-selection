local helpUI = import("/Mods/FilterSelection/Modules/HelpModules/HelpUserinterface.lua")
local helpTex = import("/Mods/FilterSelection/Modules/HelpModules/HelpTexture.lua")
local layoutHelpers = import('/lua/maui/layouthelpers.lua') 

function CreateFilterButton(parent, iconname)

	-- Get the paths to the generic button feedback icons.
	local normal, hover, down, up
	normal = helpTex.textureGenericNormal
	hover = helpTex.textureGenericOver
	down = helpTex.textureGenericDown
	up = helpTex.textureGenericUp

	-- Create the generic button.
	local UIGroup = helpUI.CreateButton(parent, normal, down, hover, up)
	UIGroup.Depth:Set(42)
	UIGroup.Width:Set(34)
	UIGroup.Height:Set(34)
	layoutHelpers.AtRightTopIn(UIGroup, parent, 0, 0)

	-- Create the actual icon.
	UIGroup["bitmap"] = helpUI.CreateBitmap(UIGroup, helpTex.GetPathIcon(iconname))
	UIGroup["bitmap"].Depth:Set(2)
	UIGroup["bitmap"].Width:Set(34)
	UIGroup["bitmap"].Height:Set(34)
	layoutHelpers.AtRightTopIn(UIGroup["bitmap"], UIGroup, 0, 0)

	-- Create the borders.
	UIGroup["neutral"] = helpUI.CreateBitmap(UIGroup, helpTex.textureNeutral)
	UIGroup["neutral"].Depth:Set(1)
	layoutHelpers.AtRightTopIn(UIGroup["neutral"], UIGroup, -8, -14)

	UIGroup["filterout"] = helpUI.CreateBitmap(UIGroup, helpTex.textureFilterOut)
	UIGroup["filterout"].Depth:Set(0)
	layoutHelpers.AtRightTopIn(UIGroup["filterout"], UIGroup, -8, -14)
	
	UIGroup["filterin"] = helpUI.CreateBitmap(UIGroup, helpTex.textureFilterIn)
	UIGroup["filterin"].Depth:Set(0)
	layoutHelpers.AtRightTopIn(UIGroup["filterin"], UIGroup, -8, -14)

	-- Create the generic functionality of a button.
	UIGroup.Active = false

	UIGroup.ChangeState = function()
		UIGroup["neutral"].Depth:Set(0)
		UIGroup["filterin"].Depth:Set(0)
		UIGroup["filterout"].Depth:Set(0)
		if not UIGroup.Active then
			UIGroup["neutral"].Depth:Set(1)
		else
			local string = tostring(import("/Mods/FilterSelection/Modules/Functionality.lua").functionality)
			UIGroup[string].Depth:Set(1)
		end
	end

	UIGroup.OnHide = function (self, hidden)
		if not hidden then
			self.ChangeState()
		end
	end

	return UIGroup

end