local UIUtil = import('/lua/ui/uiutil.lua')
local Bitmap = import('/lua/maui/bitmap.lua').Bitmap
local Button = import('/lua/maui/button.lua').Button
local Control = import('/lua/maui/control.lua').Control

-- Creates a bitmap.
-- parent: The UI element that is the parent of this UI element. Position is relative to the UI element.
-- normal: The path to the normal icon of this bitmap.
function CreateBitmap(parent, normal)

	bit = Bitmap(parent)
	bit:SetTexture(RetrieveIcon(normal))

	return bit

end

-- Creates a button.
-- parent: The UI element that is parent of this UI element. Position is relative to the parent.
-- normal: The way the button looks generally.
-- hover: The way the button looks when the mouse is over it.
-- down: the way the button looks when clicked.
-- up: the way the button looks right after clicking and right before going back to the normal / hover state.
function CreateButton(parent, normal, hover, down, up)

	button = Button(parent, RetrieveIcon(normal), RetrieveIcon(hover), RetrieveIcon(down), RetrieveIcon(up) )
	return button

end

-- Retrieves the actual icon from the path.
function RetrieveIcon(iconpath)

    if DiskGetFileInfo(iconpath) == false then
    	LOG("Helpuserinterface: Can't find icon in path: " .. iconpath)
        iconpath = '/textures/ui/common/icons/units/default_icon.dds'
    end

    icon = UIUtil.UIFile(iconpath)
    return icon

end