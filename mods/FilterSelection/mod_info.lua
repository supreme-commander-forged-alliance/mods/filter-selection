name = "Filter Selection"
version = 1
copyright = "CC BY"
description = "Helps with (not) selecting certain types of units. Have a look at the config file for hotkey information and to add your own filters!"
author = "Jip"
url = "http://81.207.237.170/Jip/SC_FilterSelection.git"
uid = "8e955f9e-48b2-11e6-beb8-9e71128cae77"

exclusive = false
ui_only = true
selectable = true
enabled = true

requires = {}
requiresNames = {}
conflicts = {}
before = {}
after = {}