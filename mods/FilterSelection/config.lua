--------------------------------------------------------------------------------------------------------------
-- WARNING: Changing this file may crash your game when you do bad changes!
-- Change with caution - look closely at what has been made before you.
-- Do not come to me (the author) for questions. There should be enough documentation around to get you going.

-- If you do experience crashes, simply redownload the mod to go back to the default.
--------------------------------------------------------------------------------------------------------------


-- This is the offset of the starting point of the interface, from the right side of the screen.
startX = 400

-- What button (combination) should be used for filtering.
filterKeybinding = "F"

-- What button (combination) should be used for switching the filter type.
filterSwitch = "Shift-F"

-- What button (combination) should be used for resetting the filter.
filterReset = "Shift-Z"

filters = 
{
	-- Template:
	-- { name = "Name", keybinding = "Key-Binding", cat = categories.XYZ },
	-- 'name' 		-> The name of this filter. Is used in the game as the name of the tooltip.
	-- 'description'-> The extra information on this filter. Is used in the game as the body of the tooltip.							
	-- 'keybinding' -> The key (combination) that is required to set this filter.				For more information about keybindings, 	see info/keybindings.txt.
	-- 'cat' 		-> The catogories that should be used in the filter. 						For more information about categories, 		see info/categories.txt.
	-- 'icon' 		-> The icon that should be used in the interface of the game. 				For more information about icons, 			see info/icons.txt.

	{ name = "Toggle Air", description = 'Filters all the air units.', keybinding = "D-1", cat = categories.AIR, icon = "air" },
	{ name = "Toggle Land", description = 'Filters all the land units.', keybinding = "D-2", cat = categories.LAND, icon = "land"},
	{ name = "Toggle Naval", description = 'Filters all the naval units.', keybinding = "D-3", cat = categories.NAVAL, icon = "sea"},
	{ name = "Toggle Hover", description = 'Filters all the hover units.', keybinding = "D-4", cat = categories.HOVER, icon = "amph"},
	{ name = "Toggle ASF", description = 'Filters all the ASF.', keybinding = "D-5", cat = categories.AIR * categories.TECH3 * categories.ANTIAIR, icon = "UAA0303_icon"},
	{ name = "Toggle Shields", description = 'Filters all the ASF.', keybinding = "D-5", cat = (categories.SHIELD * categories.MOBILE) - categories.COMMAND, icon = "UAL0307_icon"},
}